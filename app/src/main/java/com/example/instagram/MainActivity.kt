package com.example.instagram

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomNavView : BottomNavigationView = findViewById (R.layout.)
        )
        val controller = findNavController(R.id.nav_host_fragment)

        val fragmentSet = setOf<Int>(
            R.id.fragmentHome2 ,
            R.id.fragmentSearch2,
            R.id.fragmentReels2 ,
            R.id.fragmentMarket2,
            R.id.fragmentProfile2
        )

        setupActionBarWithNavController(controller , AppBarConfiguration(fragmentSet) )
        bottomNavView.setupWithNavController(controller)
    }
}